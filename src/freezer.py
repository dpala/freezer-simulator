import argparse
import numpy as np
from collections import defaultdict

def main():
    parser = argparse.ArgumentParser(description='Freezer simulator')
    parser.add_argument('-f', '--trace-files', dest='trace_file', type=argparse.FileType('r'))
    parser.add_argument('-b', '--block-size', dest='block_size', type=int, default=32)
    #parser.add_argument('--random-intervals', dest='random_intervals', type=bool, action='store_true')
    parser.add_argument('-i', '--interval-size', dest='interval_size', type=int, default=10**7)

    args = parser.parse_args()
    
    to_backup = set() # defaultdict(bool)
    block_set = set()
    
    interval = 0
    #interval_size = np.random.randint(10**7) if args.random_intervals else 10**7 #args.interval_size
    interval_size = args.interval_size
    stores = 0
    tot = 0 

    shift = int(np.log2(args.block_size))
    print('shift: ', shift)

    with args.trace_file as trace_file:
        for line in trace_file:
            cycle, op, op_size, addr = line.split(',')
            cycle = int(cycle)
            addr = int(addr) & 0xffffffff
            
            block = addr >> shift 
            block_set.add(block)
            if op == 'ST':
                to_backup.add(block) 
                stores += 1 

            tot += 1
            
            if interval != (cycle // interval_size):
                backup = len(to_backup) * (args.block_size // 4)   
                print(f'{interval} -> stores = {stores}, backup = {backup}')
                interval += 1 
                to_backup.clear()
                stores = 0

    backup = len(to_backup) * (args.block_size // 4)   
    print(f'{interval} -> stores = {stores}, backup = {backup}')
    print('max len(to_backup) =', len(block_set))

    
if __name__ == '__main__':
    main()
